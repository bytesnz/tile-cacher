import { createReadStream } from 'fs';
import polka from 'polka';
import middleware from 'tile-cacher';
import { options } from './options.js';
import { dirname, join } from 'path';
import { fileURLToPath } from 'url';

const __dirname = dirname(fileURLToPath(import.meta.url));

const app = polka();

app.get('/', (req, res) => {
	const stream = createReadStream(join(__dirname, 'index.html'));
	res.setHeader('Content-Type', 'text/html');
	stream.pipe(res);
});
app.get('/tiles/:id/:z/:x/:y.png', middleware(options));

app.listen(3030, () => {
	console.log('tile-cacher middleware listening on port 3030');
});
