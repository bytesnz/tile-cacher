module.exports = {
	'{src/README.md,CHANGELOG.md,example/*.js,package.json}': [
		() => 'repo-utils/mdFileInclude.cjs src/README.md README.md',
		'git add README.md'
	],
	'*.{css,md,json}': ['prettier --write'],
	'*.{cjs,js,svelte,ts}': ['prettier --write', 'eslint -c .eslintrc.commit.cjs --fix']
};
